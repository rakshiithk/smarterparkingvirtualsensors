var express = require('express'),
routes = require('./routes'), 
user = require('./routes/user'), 
http = require('http'), 
path = require('path'), 
virtualsensors = require('./routes/virtualsensors'), 
dashboard = require('./routes/dashboard'), 
mqtt = require('mqtt'), 
androidRoutes = require('./routes/androidRoutes'), 
mysql = require('mysql');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);

// Exposing the public folder to be accessed
app.use(express.static(path.join(__dirname, 'public')));
app.use('/public', express.static(__dirname + "/public"));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/dashboard', dashboard.dashboard);
app.get('/getdeviceList', dashboard.getdeviceList);// Proxy Call from Client
app.get('/addNewDevice', virtualsensors.addNewDevice);
app.get('/VisualizeSensorData', dashboard.VisualizeSensorData);
app.get('/getDevicesList', dashboard.getDevicesList);
app.get('/getparkinginfo', androidRoutes.getparkinginfo);

app.post('/getDeviceCurrentStatus', dashboard.getDeviceCurrentStatus);
app.post('/startPolling', virtualsensors.startDevicePolling);
app.post('/stopPolling', virtualsensors.stopPolling);
app.post('/setPollingIntr', virtualsensors.setPollingIntr);
app.post('/registerDevice', virtualsensors.registerDevice);
app.post('/getDeviceStatus', virtualsensors.getDeviceStatus);
app.post('/setParkingCapacity', virtualsensors.setParkingCapacity);
app.post('/setRate', virtualsensors.setRate);
app.post('/setParkingSpotCount', virtualsensors.setParkingSpotCount);

http.createServer(app).listen(
app.get('port'),
function() {
	console.log('Dashboard/Virtual Sensor server listening on port '
			+ app.get('port'));
});
