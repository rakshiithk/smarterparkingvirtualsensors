var smarterParkingDashboardApp = angular.module('smarterParkingDashboardApp',[]);
smarterParkingDashboardApp.controller('dashboardController', function($scope,$http) {
	
	$scope.deviceList ={};
	$scope.interval = {};
	$scope.capacity = {};
	$scope.error = {};
	$scope.search = {};
	$scope.devicestatus = {};
	$scope.Rate = {};
	$scope.emptyspots = {};
	
	//Get the Device Status Information
	$scope.getDeviceStatus = function(device,i){
		console.log("getDeviceStatus ii ==> " + i);
		console.log("getDeviceStatus device ==> " + JSON.stringify(device));
		$scope.deviceList[i].devicestatus = {};
		$http({
			method: 'POST',
			url: '/getDeviceStatus',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("getDeviceStatus Response got from the Server ==>" + JSON.stringify(response));
			$scope.deviceList[i].devicestatus = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Getting the Status inforamtion for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};

	$scope.toggleAailable = function(device){
		if(device.empty_spots == 0){
			device.empty_spots = device.capacity;
		}else{
			device.empty_spots = 0;
		}
		$http({
			method: 'POST',
			url: '/setParkingSpotCount',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("setParkingSpotCount Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Settting the setParkingCapacity for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};
	
	//Get Device List Call to server to retrieve the list of the devices from the IOT Server
	$scope.getdeviceList = function(){
		$http({
			method: 'GET',
			url: '/getdeviceList',
			headers: {
				'Content-Type': 'application/json'
			}//,
		//data: JSON.stringify($scope.user)
		}).then(function successCallback(response) {
			console.log("Response got from the Server ==>" + JSON.stringify(response));
			$scope.deviceList = response.data;
			var arrayLength = $scope.deviceList.length;
			console.log("arrayLength==> " + arrayLength);
			for (var i = 0; i < arrayLength; i++) {
				$scope.getDeviceStatus($scope.deviceList[i],i);
			    //Do something
			}
			$scope.error.errorInForm = false;
		}, function errorCallback(response) {
			$scope.deviceList.errorMessage = "Error getting the Devices list!! Please try Again..";
			$scope.deviceList.errorInForm = true;
		});
	};

	//To Be Implemented
	$scope.getDeviceCurrentStatus = function(device){
		$http({
			method: 'POST',
			url: '/getDeviceCurrentStatus',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Getting the Device's Status!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};

	//startPooling
	$scope.startPolling = function(device){
		$http({
			method: 'POST',
			url: '/startPolling',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Starting Polling for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};

	//stopPolling to stop pooling of Device
	$scope.stopPolling = function(device){
		$http({
			method: 'POST',
			url: '/stopPolling',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error stoping Polling for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};

	//setPollingIntr to stop pooling of Device
	$scope.setPollingIntr = function(device){
		console.log("interval ==> " + JSON.stringify($scope.interval));
		device.poll_interval = $scope.interval.poll_interval*1000;
		$http({
			method: 'POST',
			url: '/setPollingIntr',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("setPollingIntr Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Settting the Polling Interval for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};
	
	//setParkingCapacity to set parking of Device
	$scope.setParkingCapacity = function(device){
		device.capacity = $scope.capacity.cap;
		$http({
			method: 'POST',
			url: '/setParkingCapacity',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("setParkingCapacity Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Settting the setParkingCapacity for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};
	
	//setRate to set Rate of Device
	$scope.setRate = function(device){
		device.rate = $scope.Rate.rate;
		$http({
			method: 'POST',
			url: '/setRate',
			headers: {
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(device)
		}).then(function successCallback(response) {
			console.log("setRate Response got from the Server ==>" + JSON.stringify(response));
			//$scope.deviceList = response.data;
		}, function errorCallback(response) {
			$scope.error.errorMessage = "Error Settting the setRate for the Device!! Please try Again..";
			$scope.error.errorInForm = true;
		});
	};
	
	setInterval(function(){ 
		var arrayLength = $scope.deviceList.length;
		console.log("arrayLength==> " + arrayLength);
		for (var i = 0; i < arrayLength; i++) {
			$scope.getDeviceStatus($scope.deviceList[i],i);
		    //Do something
		}
	}, 12000);
	
});
