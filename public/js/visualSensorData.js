var map;
var infoWindow;
var markersData = [];

try{
	function toggleAvailability(device){
		/*var device;
			 for (var i =0; i < markerArray.length; i++) {
				if(deviceid == $scope.deviceList[i]){
					$scope.device = $scope.deviceList[i];
					break;
				}
			}*/
		console.log(toggleAvailability)
		/*	 $.ajax({
			            url: "/setParkingSpotCount",
			            type: "post",
			            data: device,
			            headers: {
			            	"Content-Type": "application/json"
			            },
			            dataType: "json",
			            success: function (data) {
			                console.info(data);
			            }
			        });*/
	}

	function initialize() {
		$.get(
				"/getDevicesList",
				function(data) {
					markersData = JSON.parse(data);
					console.log(markersData);
					displayMarkers();
				}
		);
		var mapOptions = {
				//center : new google.maps.LatLng(40.601203, -8.668173),
				zoom : 9,
				mapTypeId : 'roadmap',
		};

		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		infoWindow = new google.maps.InfoWindow();

		google.maps.event.addListener(map, 'click', function() {
			infoWindow.close();
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);

	function displayMarkers() {

		var bounds = new google.maps.LatLngBounds();

		for (var i = 0; i < markersData.length; i++) {
			console.log(markersData[i]);
			var latlng = new google.maps.LatLng(markersData[i].latitude,markersData[i].longitude);
			var id_no = markersData[i].id_no;
			var device_id = markersData[i].device_id;
			var device_type = markersData[i].device_type;
			var capacity = markersData[i].capacity;
			var empty_spots = markersData[i].empty_spots;
			createMarker(latlng, device_id, device_type, capacity, empty_spots,id_no);
			bounds.extend(latlng);
		}
		map.fitBounds(bounds);
	}

	function drop() {
		for (var i =0; i < markerArray.length; i++) {
			setTimeout(function() {
				addMarkerMethod();
			}, i * 200);
		}
	}

	function toggleBounce() {
		if (marker.getAnimation() !== null) {
			marker.setAnimation(null);
		} else {
			marker.setAnimation(google.maps.Animation.BOUNCE);
		}
	}

	function createMarker(latlng, device_id, device_type, capacity, empty_spots,id_no) {
		if(device_type == "CAR_PARK_SENSOR"){
			var image = '/images/parkingIcon1.png';
		}else{
			var image = '/images/rsz_garage-512.png';
		}

		var marker = new google.maps.Marker({
			map : map,
			position : latlng,
			title : device_id,
			animation: google.maps.Animation.DROP,
			icon: image
		});

		google.maps.event.addListener(marker, 'click', function() {

			var iwContent = '<div id="iw_container">' +'<div class="iw_title">Parking Sensor ID : '
			+ device_id + '</div>' + '<div class="iw_content">Parking Type : ' + device_type
			+ '<br />Parking Sensor Name : ' + id_no
			+ '<br /> Parking Capacity : ' + capacity + '<br /> Parking Spot Left : ' + empty_spots 
			//+ '<br/><button type="button" class="btn btn-danger" onclick="alert("test")">Toggle Sensor On/Off</button> </div></div>';

			infoWindow.setContent(iwContent);

			infoWindow.open(map, marker);
		});
	}	
}catch(e){
	console.log(e.stack)
}
