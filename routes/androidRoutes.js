var device;
var self = this;

function distance(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var radlon1 = Math.PI * lon1/180
	var radlon2 = Math.PI * lon2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return dist
}

exports.getparkinginfo = function(req,res){
	global.mongodb.collection('devicedata').find({}).toArray(function(err, results) {
		if(err)
			res.status(404).send(err);
		else{
			var radius = parseFloat(req.query.radius);
			var latitude = parseFloat(req.query.latitude);
			var longitude = parseFloat(req.query.longitude);
			
			console.log("radius ==> "+ radius);
			console.log("latitude ==> "+ latitude);
			console.log("longitude ==> "+ longitude);
			
			var FinalResult = [];
			var dist;
			for(var i = 0; i < results.length;i++ ){
				console.log(results[i].device_id + " == > results Info :: " + results[i].latitude + " :: " + results[i].longitude);
				if(results[i].empty_spots > 0){
					dist = distance(results[i].latitude, results[i].longitude, latitude, longitude, "M");
					console.log("Distance ==> " + dist);
					if(dist <= radius ){
						results[i].distance = dist;
						console.error("Sensor " + results[i].device_id +" is in the vicinity of the point!!!");
						FinalResult.push(results[i]);
					}
				}
			}
			res.status(200).send(FinalResult);
		}	
	});	
};