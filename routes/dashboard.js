var smarterParkingServerURL = "http://smarterparkingiotserver.mybluemix.net";
var mongo = process.env.VCAP_SERVICES;
var port = process.env.PORT || 3030;
var conn_str = "";

if (mongo) {
	var env = JSON.parse(mongo);
	if (env['mongodb-2.4']) {
		mongo = env['mongodb-2.4'][0]['credentials'];
		if (mongo.url) {
			conn_str = mongo.url;
		} else {
			console.log("No mongo found");
		}  
	} else {
		conn_str = 'mongodb://localhost:27017';
	}
} else {
	conn_str = 'mongodb://localhost:27017';
}

var MongoClient = require('mongodb').MongoClient;
var db; 
MongoClient.connect(conn_str, function(err, database) {
	if(err) throw err;
	db = database;
	global.mongodb =  db; 
	console.log("conn_str ==> " + conn_str + " connected to db!!");
}); 

exports.dashboard = function(req,res){
	res.render('dashboard', { title: 'Express' });	
};

//Return
exports.getdeviceList = function(req,res){
	try{
		db.collection('devicedata').find({}, function(err, cursor) {
			if (err) {
				console.log(err.stack); 
				res.status(404).send('mongodb cursor to array failed');
			} else {
				cursor.toArray(function(err, devices) {
					if (err) {
						console.log(err.stack); 
						res.status(404).send('mongodb cursor to array failed');
					} else {
						
						res.status(200).send(devices);
					}
				});
			}
		});
	}catch(e){
		console.error("getdeviceList Error : " + e);
	}
};

exports.getDeviceCurrentStatus = function(req,res){
	try{
		var http = require("http");
		http.get(smarterParkingServerURL + '/getDeviceCurrentStatus', function (response) {
			var data = "";
			response.on("data", function (chunk) {
				data += chunk;
			});
			response.on("end", function () {
				console.log(data);
				res.status(200).send(data);
			});
		});	
	}catch(e){
		console.error("getDeviceCurrentStatus Error : " + e);
	}
};

exports.VisualizeSensorData = function(req,res){
	try{
		res.render('VisualizeSensorData', { title: 'Express' });	
	}catch(e){
		console.error("VisualizeSensorData Error : " + e);
	}
};

exports.getDevicesList = function(req,res){
	try{
		var request = require('request');			
		request({
			url: "http://smarterparkingiotserver.mybluemix.net/getdeviceList",
			method: "GET",
			headers: {
				"content-type": "application/x-www-form-urlencoded",  // <--Very important!!!
			}/*,   // <--Very important!!!
			body: data*/
		}, function (error, response, body){
			console.log(body);
			res.status(200).send(body);
		});
	}catch(e){
		console.error("VisualizeSensorData Error : " + e);
	}
};
