var mysql = require('mysql');
var dbconnection;
exports.getNewConnectionFromDB = function(){
	try{
		dbconnection = mysql.createConnection({
			multipleStatements: true,
			host : 'us-cdbr-iron-east-03.cleardb.net',
			user : 'b4c304fcb4938d',
			password : '788e7584',
			database: 'ad_e0f4d2f49b03e03',
			connectTimeout: 6000,
			waitForConnections: true,
			pool: false,
		});	
		return dbconnection;
	}catch(e){
		console.log("getNewConnectionFromDB ==> " + e);
	}
};

exports.terminateConnection = function(){
	try{
		dbconnection.destroy();
	}catch(e){
		console.log("TerminateConnection ==> " + e);
	}


};