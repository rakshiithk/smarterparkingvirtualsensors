var mqtt = require('mqtt');

var client = {};
var PollingSensors = {};
var bluemixIOTURL = "messaging.internetofthings.ibmcloud.com";
var IOTServicePort = 1883;
var IOTEventStatusQueue = "iot-2/evt/status/fmt/json";
var defaultPollingInterval = 2000;
var currentOrgId = "e4eeeh";
var IOTWebServiceEndPoint = "internetofthings.ibmcloud.com";

function getPayloadParkingSensor(device) {
	var payload = {};
	try {
		payload = {
			"_id" : device.device_id,
			"device_id" : device.device_id,
			"id_no" : device.id_no,
			"device_type" : device.device_type,
			"capacity" : device.capacity,
			"empty_spots" : device.empty_spots,
			"latitude" : device.latitude,
			"longitude" : device.longitude,
			"rate" : device.rate,
			"apiToken" : device.apiToken,
			"apiKey" : device.apiKey,
			"org" : device.org,
			"virtual_sensor" : true,
			"polling_interval" : device.polling_interval
		};
	} catch (e) {
		console.error("getPayloadParkingSensor Error XXXXXXXXXX:XXXXXXXXXXX "
				+ e);
	}
	// console.log("Virtual Payload == > " + JSON.stringify(payload));
	return payload;
}

function startPolling(device, mqttHost, mqttPort) {
	try {
		if (client[device.device_id] == null) {
			var clientId = [ "d", device.org, device.device_type,
					device.device_id ].join(':');
			// console.log("clientId ==> " + clientId);
			client[device.device_id] = mqtt.connect("mqtt://" + device.org
					+ "." + mqttHost + ":" + mqttPort, {
				"clientId" : clientId,
				"keepalive" : 30,
				"username" : device.apiKey,
				"password" : device.apiToken
			});
		}

		// Callback to handle after the MQTT is connected!!
		if (client[device.device_id] != null) {
			console
					.log('MQTT Connection to IBM Bluemix IOT Service is Sucessfull!!');
			client[device.device_id].publish(IOTEventStatusQueue, JSON
					.stringify(getPayloadParkingSensor(device)));
			console.log("Data sent by device ==> " + device.device_id);
		}

		// Callback to handle if the MQTT connection Error's out!!
		client[device.device_id].on('error', function(err) {
			console.error('client error' + err);
		});

		// Callback to handle if the MQTT connection Error's out!!
		client[device.device_id].on('close', function() {
			console.log('client closed');
		});
	} catch (e) {
		console.error("startPolling Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}
}

function DevicePolling(device) {
	try {
		startPolling(device, bluemixIOTURL, IOTServicePort);
	} catch (e) {
		console.error("DevicePolling ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}
}

exports.startDevicePolling = function(req, res) {
	try {
		var device = req.body;
		if (device.virtual_sensor == true) {
			PollingSensors[device.device_id] = setInterval(function() {
				DevicePolling(device);
			}, defaultPollingInterval);
			console
					.log("Start polling on the VIRTUAL Device ==> "
							+ JSON.stringify(device)
							+ ", initial Pooling Freqency is 2 Seconds!! and the Interval Function is set on ==> "
							+ PollingSensors[device.device_id]);
		} else {
			console.log("Start polling on the REAL Device ==> "
					+ JSON.stringify(device)
					+ ", initial Pooling Freqency is 2 Seconds!!");

			var request = require('request');
			var data = "deviceId=" + device.device_id + "&deviceType="
					+ device.device_type + "&eventOrCommandType=devcmd"
					+ "&format=String" + "&payload=startBroadcast";

			request(
					{
						url : "http://smarterparkingiotserver.mybluemix.net/startPolling",
						method : "POST",
						headers : {
							"content-type" : "application/x-www-form-urlencoded", // <--Very
																					// important!!!
						}, // <--Very important!!!
						body : data
					}, function(error, response, body) {
						console.log(body);
					});
		}

	} catch (e) {
		console.error("startPolling ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}
	res.status(200).send("startDevicePolling Interval of the devices");
};

exports.stopPolling = function(req, res) {
	try {
		var device = req.body;
		if (device.virtual_sensor == true) {
			console
					.log("STOP polling on the VIRTUAL Device ==> "
							+ JSON.stringify(device)
							+ ", initial Pooling Freqency was 2 Seconds!! and the Interval Function is set on ==> "
							+ PollingSensors[device.device_id]);
			clearInterval(PollingSensors[device.device_id]);
		} else {
			console.log("Stop polling on the REAL Device ==> "
					+ JSON.stringify(device)
					+ ", initial Pooling Freqency is 2 Seconds!!");
			var request = require('request');
			var data = "deviceId=" + device.device_id + "&deviceType="
					+ device.device_type + "&eventOrCommandType=devcmd"
					+ "&format=String" + "&payload=stopBroadcast";
			request(
					{
						url : "http://smarterparkingiotserver.mybluemix.net/stopPolling",
						method : "POST",
						headers : {
							"content-type" : "application/x-www-form-urlencoded", // <--Very
																					// important!!!
						}, // <--Very important!!!
						body : data
					}, function(error, response, body) {
						console.log(body);
					});
		}
	} catch (e) {
		console.error("stopPolling ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}
	res.status(200).send("stopPolling Interval of the devices");
};

exports.setPollingIntr = function(req, res) {
	try {
		var device = req.body;
		if (device.virtual_sensor == true) {
			console
					.log("Res polling on the Device ==> "
							+ JSON.stringify(device)
							+ ", initial Pooling Freqency was 2 Seconds!! and the Interval Function is set on ==> "
							+ PollingSensors[device.device_id]);
			clearInterval(PollingSensors[device.device_id]);

			PollingSensors[device.device_id] = setInterval(function() {
				DevicePolling(device);
			}, device.poll_interval);

			global.mongodb.collection('devicedata').updateOne({
				"_id" : device.device_id
			}, {
				$set : {
					"polling_interval" : device.poll_interval
				},
				$currentDate : {
					"lastModified" : true
				}
			}, function(err, results) {
				console.log(results);
			});
		} else {
			console.log("setPollingIntr on the REAL Device ==> "
					+ JSON.stringify(device)
					+ ", initial Pooling Freqency is 2 Seconds!!");
			var request = require('request');
			var data = "deviceId=" + device.device_id + "&deviceType="
					+ device.device_type + "&eventOrCommandType=devcmd"
					+ "&format=String" + "&payload=setPollingIntr/"
					+ device.poll_interval;
			request(
					{
						url : "http://smarterparkingiotserver.mybluemix.net/setPollingIntr",
						method : "POST",
						headers : {
							"content-type" : "application/x-www-form-urlencoded", // <--Very
																					// important!!!
						}, // <--Very important!!!
						body : data
					}, function(error, response, body) {
						console.log(body);

					});
		}
	} catch (e) {
		console.error("stopPolling ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}
	res.status(200).send("Polling Interval of the devices has been Updated");
};

exports.setParkingCapacity = function(req, res) {
	try {
		var device = req.body;
		if (device.virtual_sensor == true) {
			global.mongodb.collection('devicedata').updateOne({
				"_id" : device.device_id
			}, {
				$set : {
					"capacity" : device.capacity
				}
			}, function(err, results) {
				console.error("setParkingCapacity======================>");
				console.error(err);
				console.error(results)
			});
		} else {
			console.log("setParkingCapacity on the REAL Device ==> "
					+ JSON.stringify(device));
			var request = require('request');
			var data = "deviceId=" + device.device_id + "&deviceType="
					+ device.device_type + "&eventOrCommandType=devcmd"
					+ "&format=String" + "&payload=setCapacity/"
					+ device.capacity;
			request(
					{
						url : "http://smarterparkingiotserver.mybluemix.net/setParkingCapacity",
						method : "POST",
						headers : {
							"content-type" : "application/x-www-form-urlencoded", // <--Very
																					// important!!!
						}, // <--Very important!!!
						body : data
					}, function(error, response, body) {
						console.log(body);

					});
		}
	} catch (e) {
		console.error("setParkingCapacity ===> Error XXXXXXXXXX:XXXXXXXXXXX "
				+ e);
	}
	res.status(200).send("Parking Capacity of the devices has been Updated");
};

exports.setRate = function(req, res) {
	try {
		var device = req.body;
		if (device.virtual_sensor == true) {
			global.mongodb.collection('devicedata').updateOne({
				"_id" : device.device_id
			}, {
				$set : {
					"rate" : device.rate
				}
			}, function(err, results) {
				console.error("setRat======================>");
				console.error(err);
				console.error(results)
			});
		} else {
			console.log("setParkingCapacity on the REAL Device ==> "
					+ JSON.stringify(device));
			var request = require('request');
			var data = "deviceId=" + device.device_id + "&deviceType="
					+ device.device_type + "&eventOrCommandType=devcmd"
					+ "&format=String" + "&payload=setRate/" + device.rate;
			request({
				url : "http://smarterparkingiotserver.mybluemix.net/setRate",
				method : "POST",
				headers : {
					"content-type" : "application/x-www-form-urlencoded", // <--Very
																			// important!!!
				}, // <--Very important!!!
				body : data
			}, function(error, response, body) {
				console.log(body);

			});
		}
	} catch (e) {
		console.error("setParkingCapacity ===> Error XXXXXXXXXX:XXXXXXXXXXX "
				+ e);
	}
	res.status(200).send("Parking Capacity of the devices has been Updated");
};

exports.setParkingSpotCount = function(req, res) {
	try {
		var device = req.body;
		if (device.virtual_sensor == true) {
			global.mongodb.collection('devicedata').updateOne({
				"_id" : device.device_id
			}, {
				$set : {
					"empty_spots" : device.empty_spots
				}
			}, function(err, results) {
				console.error("empty_spots======================>");
				console.error(err);
				console.error(results)
			});
		} else {
			console.log("setParkingSpotCount on the REAL Device ==> "
					+ JSON.stringify(device));
			var request = require('request');
			var data = "deviceId=" + device.device_id + "&deviceType="
					+ device.device_type + "&eventOrCommandType=devcmd"
					+ "&format=String" + "&payload=setRate/" + device.rate;
			request(
					{
						url : "http://smarterparkingiotserver.mybluemix.net/setParkingSpotCount",
						method : "POST",
						headers : {
							"content-type" : "application/x-www-form-urlencoded", // <--Very
																					// important!!!
						}, // <--Very important!!!
						body : data
					}, function(error, response, body) {
						console.log(body);
					});
		}
	} catch (e) {
		console.error("setParkingCapacity ===> Error XXXXXXXXXX:XXXXXXXXXXX "
				+ e);
	}
	res.status(200).send("Parking Capacity of the devices has been Updated");
};

exports.getDeviceStatus = function(req, res) {
	try {
		var request = require('request');
		var device = req.body;
		var data = "deviceId=" + device.device_id;
		request(
				{
					url : "http://smarterparkingiotserver.mybluemix.net/getDeviceStatus",
					method : "POST",
					headers : {
						"content-type" : "application/x-www-form-urlencoded", // <--Very
																				// important!!!
					}, // <--Very important!!!
					body : data
				}, function(error, response, body) {
					console.log(body);
					res.status(200).send(body);
				});
	} catch (e) {
		console.error("getDeviceStatus ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}

};

exports.addNewDevice = function(req, res) {
	try {
		res.render('addNewDevice');
	} catch (e) {
		console.error("addNewDevice ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	}
};

exports.registerDevice = function(req, res) {
	try {
		var device = req.body;
		var payload = {};
		payload = {
			// "_id" : device.device_id,
			"device_id" : device.device_id,
			"id_no" : device.id_no,
			"device_type" : device.device_type,
			"capacity" : device.capacity,
			"empty_spots" : device.capacity,
			"latitude" : device.latitude,
			"longitude" : device.longitude,
			"rate" : device.rate,
			"apiToken" : "use-token-auth",
			"apiKey" : device.apiKey,
			"org" : currentOrgId,
			"virtual_sensor" : device.virtual_sensor,
			"polling_interval" : 2000
		};

		console.log("Payload == > " + JSON.stringify(payload));

		var endPoint = IOTWebServiceEndPoint + '/api/v0001/organizations/'
				+ currentOrgId + '/devices';
		console.log("Register New Device ==> " + JSON.stringify(device)
				+ " ==> Endpoint ==> " + endPoint);

		var mysql = require('./mysql');
		var sql_stmt = "INSERT INTO `ad_e0f4d2f49b03e03`.`virtual_sensors` SET ?";
		var connection = mysql.getNewConnectionFromDB();
		connection.query(sql_stmt, payload, function(error, results, fields) {
			mysql.terminateConnection();
			connection = null;
			if (!error) {
				console.log("Device Registered Sucessfully");
			} else {
				console.log("Device Registeration Failed!!" + error)
			}

			payload._id = device.device_id, global.mongodb.collection(
					'devicedata').insertOne(payload, function(err, cursor) {
				if (err) {
					console.error("registerDevice======================>");
					console.error(err);
					console.error(results)
					console.log(err.stack);
					//res.status(404).send('mongodb cursor to array failed');
				} else {
					console.log(cursor);
				}
				res.render('dashboard');
			});
		});
	} catch (e) {
		if (connection != null)
			connection.end();
		console.error("registerDevice ===> Error XXXXXXXXXX:XXXXXXXXXXX " + e);
	} finally {
	}
};
